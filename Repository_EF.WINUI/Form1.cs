﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Repository_EF.DAL.Repository;
using Repository_EF.DATA.Model_Entity;

namespace Repository_EF.WINUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Repository<Category> cateRep = new Repository<Category>();
            cmbKategoriler.DataSource = cateRep.GetAll();
            cmbKategoriler.DisplayMember = "CategoryName";

            Repository<Employee> empRep = new Repository<Employee>();
            cmbCalisanlar.DataSource = empRep.GetAll();
            cmbCalisanlar.DisplayMember = "FirstName";

            Repository<Product> proRep = new Repository<Product>();
            cmbUrunler.DataSource = proRep.GetAll();
            cmbUrunler.DisplayMember = "ProductName";
        }
    }
}
