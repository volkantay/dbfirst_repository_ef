﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repository_EF.DATA.Model_Entity;
using System.Data.Entity;

namespace Repository_EF.DAL.Repository
{
    public class Repository<T>:IRepo<T> where T:class
    {

        private readonly NORTHWNDEntities _db;
        private readonly DbSet<T> _dbSet;
        public Repository()
        {
            _db = new NORTHWNDEntities();
            _dbSet = _db.Set<T>();
        }

        public List<T> GetAll()
        {
            return _dbSet.ToList();
        }

        public T GetById(int ID)
        {
            return _dbSet.Find(ID);
        }

        public bool Insert(T Entity)
        {
            _dbSet.Add(Entity);
            if (_db.SaveChanges()>0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Update(T Entity)
        {
            _dbSet.Attach(Entity);
            _db.Entry(Entity).State = EntityState.Modified;
            if (_db.SaveChanges() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool Delete(int ID)
        {
         T yakalan=   this.GetById(ID);
         _dbSet.Remove(yakalan);
         if (_db.SaveChanges() > 0)
         {
             return true;
         }
         else
         {
             return false;
         }
        }
    }
}
