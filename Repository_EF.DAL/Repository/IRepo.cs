﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository_EF.DAL.Repository
{
    interface IRepo<T> where T: class
    {
        List<T> GetAll();
        T GetById(int ID);
        bool Insert(T Entity);
        bool Update(T Entity);
        bool Delete(int ID);
   

    }
}
